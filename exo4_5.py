#exo4
a="salut"
b=102
c=10.318

print(f"{a} {b} {c}")

#exo5
perc_GC=((4500+2575)/14800)*100

print(f"Le pourcentage de GC est {perc_GC:.0f}")
print(f"Le pourcentage de GC est {perc_GC:.1f}")
print(f"Le pourcentage de GC est {perc_GC:.2f}")
print(f"Le pourcentage de GC est {perc_GC:.3f}")